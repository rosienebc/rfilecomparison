#-------------------------------------------------
#
# Project created by QtCreator 2014-12-23T22:16:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DomChatoTeste
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    rdir.c \
    rlista.c

HEADERS  += \
    rlista.h \
    rdir.h \
    mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += \
    resource.qrc
