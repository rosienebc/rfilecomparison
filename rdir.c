#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

#include "rdir.h"
#include "rlista.h"

#define MIN3(a, b, c) ((a) < (b) ? ((a) < (c) ? (a) : (c)) : ((b) < (c) ? (b) : (c)))

//recebe duas strings, e retorna suas distancias
int distancia_string(char *s1, char *s2) {
    return distancia_bytes(s1,strlen(s1),s2,strlen(s2));
}
//Recebe dois bloco de bytes e  seus respectivos tamanhos e retorna suas distancias ;)
int distancia_bytes(char* s1, unsigned int  s1len, char* s2,unsigned int s2len) {
    unsigned int x, y;
    unsigned int matrix[s2len+1][s1len+1];
    matrix[0][0] = 0;
    for (x = 1; x <= s2len; x++)
        matrix[x][0] = matrix[x-1][0] + 1;
    for (y = 1; y <= s1len; y++)
        matrix[0][y] = matrix[0][y-1] + 1;
    for (x = 1; x <= s2len; x++)
        for (y = 1; y <= s1len; y++)
            matrix[x][y] = MIN3(matrix[x-1][y] + 1, matrix[x][y-1] + 1, matrix[x-1][y-1] + (s1[y-1] == s2[x-1] ? 0 : 1));

    return(matrix[s2len][s1len]);
}
// Funcao de comparacao por nome de arquivo
float semelhanca_string_percent(char *s1, char *s2) {
    float percent;
    unsigned int tam1= strlen(s1),tam2= strlen(s2);
    //se a primeira string for a maior...
    if( tam1 > tam2 )
        //para se achar a razao da semelhanca basta pegar a distancia entre as string e dividir pela maior
        percent = 1-(float)((float)distancia_string(s1,s2)/(float)tam1);
    else
        percent = 1-((float)((float)distancia_string(s2,s1)/(float)tam2));

    //para se ter o percentual basta multiplicar a razao por 100
    return percent*100;
}
// Funcao de comparacao por conteudo de arquivo
float compara_arquivos_conteudo(char *name,char* name2) {

    FILE *file;
    FILE *file2;
    char *buffer;
    char *buffer2;
    long long fileLen,fileLen2;

    //Open file
    file = fopen(name, "rb");
    file2 = fopen(name2, "rb");
    if (!file) {
        fprintf(stderr, "\nImpossivel abrir arquivo %s", name);
        return -1;
    }
    if (!file2) {
         if (file!=NULL)
              fclose(file);
        fprintf(stderr, "\nImpossivel abrir arquivo %s", name2);
        return -1;
    }

    //Pega tamanho do primeiro arquivo
    fseek(file, 0, SEEK_END);
    fileLen=ftell(file);
    fseek(file, 0, SEEK_SET);
  //  printf("\ntamanho do fililen 1::%d",fileLen);

    //Pega tamanho do segundo arquivo
    fseek(file2, 0, SEEK_END);
    fileLen2=ftell(file2);
    fseek(file2, 0, SEEK_SET);
//    printf("\ntamanho do fililen 2::%d",fileLen2);

    //Aloca memoria
    buffer=(char *)malloc(fileLen+1);
    if (!buffer) {
        if (file!=NULL)
             fclose(file);
        if (file!=NULL)
             fclose(file2);
        fprintf(stderr, "\nErro de memoria: %s!",name);
        return -1;
    }
    buffer2=(char *)malloc(fileLen2+1);
    if (!buffer2) {
        if (file!=NULL)
             fclose(file);
        if (file!=NULL)
             fclose(file2);
        fprintf(stderr, "\nErro de memoria: %s!",name2);
        return -1;
    }

    //Ler os conteudos em buffer
    int distancia=0;
    int cont=0;
    int tam_buffer =5; // Numero de bytes que serao lidos do disco
    float semelhanca_maxima = 0.0; //Semelhanca maxima deles, zeramos de bobeira, nao precisa

    //se o primeiro arquivo for menor do que segundo
    // importante pois so comparamos os arquivos ateh onde for o menor
    if(fileLen <= fileLen2) {
        //Calcula o maximo de semelhanca que se pode ter, se eles tiverem tamanhos iguais sera 100%( 1.00 )
        semelhanca_maxima = ((float)fileLen /(float)fileLen2);
        while(!feof(file)) {
            //ler e colocar na var buffer2
            fread(buffer2, tam_buffer, 1, file2);
            //ler e colocar na var buffer
            fread(buffer,tam_buffer,1,file);
            //Acumula as distancias
            distancia  += distancia_bytes(buffer2,tam_buffer,buffer,tam_buffer);
            //Acumula os bytes comparados
            cont+=tam_buffer;

        }
     //senao...
    } else {
        //Calcula o maximo de semelhanca que se pode ter, se eles tiverem tamanhos iguais sera 100%( 1.00 )
        semelhanca_maxima = ((float)fileLen2 / (float)fileLen);
        while(!feof(file2)) {
            fread(buffer, tam_buffer, 1, file);
            fread(buffer2,tam_buffer,1,file2);
            distancia  += distancia_bytes(buffer,tam_buffer,buffer2,tam_buffer);
            cont+=tam_buffer;
        }
    }
    //Essa eh a similaridade parcial. Dos bytes comparados, quantos % de semelhanca. saka? calma
    float similaridade_parcial = 1-(float)((float)distancia/(float)cont);
    float similaridade =similaridade_parcial*semelhanca_maxima;
//    printf("\nDistancia: %d\nSimilaridade: %.2f",distancia,similaridade_parcial);
////
//    printf("\nsemelhanca maxima %.2f",semelhanca_maxima);

    free(buffer);
    free(buffer2);


    fclose(file);
    fclose(file2);

    return similaridade*100;
}


//dado um nome de um diretorio, retorna um lista com o nomes dos arquivos
RLista* get_nome_arquivos(char *diretorio) {
    RLista *rlista = criar_lista();
    DIR *dir;
    struct dirent *lsdir;
    char* aux;

    dir = opendir(diretorio);

    while ( ( lsdir = readdir(dir) ) != NULL ) {
        if(strcmp(lsdir->d_name,".") && strcmp(lsdir->d_name,"..")) {
            aux = (char*) malloc(sizeof(char)*(strlen(lsdir->d_name)+1));
            strcpy(aux,lsdir->d_name);
            insere_lista(rlista,aux);
        }
    }
    closedir(dir);

    return rlista;
}






