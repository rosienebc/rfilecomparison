#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "rlista.h"
#include "rdir.h"


#include <QFileDialog>
#include <QMessageBox>
#include <QVariant>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->init_table();

}

MainWindow::~MainWindow()
{
    delete ui;
}

//Inicializa a table so com os cabecalhos
void MainWindow::init_table()
{
    //Model na qt eh um modelo para a tabela
    model = new QStandardItemModel(0,3,this);

    //Configura o cabecalho da tabela
    model->setHeaderData(0,Qt::Horizontal,QObject::tr("Diretório #1 - Arquivos"));
    model->setHeaderData(1,Qt::Horizontal,QObject::tr("Diretório #2 - Arquivos"));
    model->setHeaderData(2,Qt::Horizontal,QObject::tr("Semelhança(%)"));

    ui->tableArquivos->setModel(model);
}
//Desenha a tabela com os dados por nome
void MainWindow::draw_table(QString dir1,QString dir2)
{
    RLista *lista_arq1,*lista_arq2;
    //Schar diretorio1[]="/Users/rosiene/Desktop/pasta1/",diretorio2[]="/Users/rosiene/Desktop/pasta4/";
    char *busca1,*busca2;

    //Transforma uma QString para um vetor de char(String em cen)
    busca1 = (char*) strdup(dir1.toLocal8Bit().constData());
    busca2 = (char*) strdup(dir2.toLocal8Bit().constData());

    //A linha comeca em 0
    int row=0;

    No *p,*q;
    // qDebug() << dir1;
    lista_arq1 = get_nome_arquivos(busca1);
    lista_arq2 = get_nome_arquivos(busca2);
    int linhas;
    linhas = lista_arq1->tam * lista_arq2->tam;
    //Pega o maior tamanho de linhas para poder desenhar a tabela
    //acho que eh mensagem sublminar ;)
    /*if(lista_arq1->tam > lista_arq2->tam)
        linhas =lista_arq1->tam;
    else
        linhas =lista_arq2->tam;*/

    //Model na qt eh um modelo para a tabela
    model = new QStandardItemModel(linhas,3,this);

    //Configura o cabecalho da tabela
    model->setHeaderData(0,Qt::Horizontal,QObject::tr("Diretório #1 - Arquivos"));
    model->setHeaderData(1,Qt::Horizontal,QObject::tr("Diretório #2 - Arquivos"));
    model->setHeaderData(2,Qt::Horizontal,QObject::tr("Semelhança(%)"));

    // lista_arq1=criar_lista();
    //Intera as duas listas de arquivos

    for(p=lista_arq1->inicio; p !=NULL ; p=p->prox )
    {
        for(q=lista_arq2->inicio; q!=NULL; q = q->prox)
        {
            float semelhanca = semelhanca_string_percent((char*)p->info,(char*)q->info);
            //Cria um objeto qIndex, que possui a da celula

            QColor corCelula = this->nivel_de_semelhanca(semelhanca);

            //Coloca os dados na    coluna 0 "Arquivo"
            QModelIndex index =model ->index(row,0,QModelIndex());
            //Configura o valor da string em c na celula
            //para transforma uma string em c para QString da qt basta usar a QString(char* str)
            model->setData(index,QString((char*)p->info));
            model->setData(index,corCelula,Qt::BackgroundColorRole);

            //Coloca os dados na coluna 1 "Arquivo"
            index =model ->index(row,1,QModelIndex());
            model->setData(index,QString((char*)q->info));
            model->setData(index,corCelula,Qt::BackgroundColorRole);

            //Coloca os dados na coluna 2 "Semelhanca"
            index =model ->index(row,2,QModelIndex());
            model->setData(index,semelhanca);
            model->setData(index,corCelula,Qt::BackgroundColorRole);
            //passa para a prox linha
            row++;
        }
    }
    //namespaced ui, saka? siim ,tem mais?.  praticamente em qualquer funcao da janela, qualquer campo que quiser chamar
    //no seu nao esta muito evidente, mas no meu, como tenho mais de uma janela. com campoNome. uso um namespaced ui
    //e outro fk por exemplo. entende? eh so para evitar conflito entendiiii
    //blza
    ui->tableArquivos->setModel(model);
    //a tabela fica renderizada para ocupar a janela, indepedentemente do conteudo
    //  ui->tableArquivos->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

//Desenha os dados na tabela por conteudo os parametros sao os diretorios
void MainWindow:: draw_table_content(QString dir1,QString dir2){
    RLista *lista_arq1,*lista_arq2;
    char *busca1,*busca2;

    //transformo qString para char*
    busca1 = (char*) strdup(dir1.toLocal8Bit().constData());
    busca2 = (char*) strdup(dir2.toLocal8Bit().constData());

    int row=0;

    No *p,*q;

    //qDebug() << dir1;
    //pega lista de arquivos, so com os nomes
    //tipo, nessa lista, meu amor. so tem os nomes dos arquivos. certo?
    lista_arq1 = get_nome_arquivos(busca1);
    lista_arq2 = get_nome_arquivos(busca2);
    int linhas;
    linhas = lista_arq1->tam * lista_arq2->tam;
    /*
    if(lista_arq1->tam > lista_arq2->tam)
        linhas =lista_arq1->tam;
    else
        linhas =lista_arq2->tam;*/

    model = new QStandardItemModel(linhas,3,this);

    model->setHeaderData(0,Qt::Horizontal,QObject::tr("Diretório #1 - Arquivos"));
    model->setHeaderData(1,Qt::Horizontal,QObject::tr("Diretório #2 - Arquivos"));
    model->setHeaderData(2,Qt::Horizontal,QObject::tr("Semelhança(%)"));


    QString caminho_absoluto1,caminho_absoluto2;
    for(p=lista_arq1->inicio; p !=NULL ; p=p->prox )
    {
        for(q=lista_arq2->inicio; q!=NULL; q =q->prox)
        {
            //caminho absoluto eh a juncao do diretorio1 mais seus arquivos
            caminho_absoluto1 = dir1+(QString((char*)p->info));
            //caminho absoluto 2 eh a juncao do diretorio1 mais seus arquivos
            caminho_absoluto2 = dir2+(QString((char*)q->info));

            float semelhanca = compara_arquivos_conteudo((char*) strdup(caminho_absoluto1.toLocal8Bit().constData()),
                                                         (char*) strdup(caminho_absoluto2.toLocal8Bit().constData()));

            QColor corCelula = nivel_de_semelhanca(semelhanca);

            QModelIndex index =model ->index(row,0,QModelIndex());
            model->setData(index,QString((char*)p->info));
            model->setData(index,corCelula,Qt::BackgroundColorRole);

            index =model ->index(row,1,QModelIndex());
            model->setData(index,QString((char*)q->info));
            model->setData(index,corCelula,Qt::BackgroundColorRole);

            index =model ->index(row,2,QModelIndex());
            model->setData(index,semelhanca);
            model->setData(index,corCelula,Qt::BackgroundColorRole);

            //passa para a prox linha
            row++;
        }
    }

    ui->tableArquivos->setModel(model);
    // ui->tableArquivos->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}
//funcao do botao indicars diretorio
void MainWindow::on_buscar1_clicked()
{
    //abre uma janela para escolher uma pasta e retorna a QString com o diretorio
    QString fileName = QFileDialog::getExistingDirectory();
    ui->campoDir1->setText(fileName);

}
//funcao do botao 2 indicar o segundo diretorio
void MainWindow::on_buscar2_clicked()
{
    //abre uma janela para escolher uma pasta e retorna a QString com o diretorio
    QString fileName1 = QFileDialog::getExistingDirectory();
    ui->campoDir2->setText(fileName1);
}

//funcao do botao para realizar busca
void MainWindow::on_pushButton_clicked()
{
    int  index = ui->tipo_pesquisa->currentIndex();
    if(ui->campoDir1->text().length() == 0 || ui->campoDir2->text().length()==0 ){
        QMessageBox::information(this, "Mensagem", "Informe um diretorio valido!", QMessageBox::Ok);
        this->init_table();
    }else{
        //para poder limpar os dados antigos
        this->init_table();
        //desativa o botao para evitar de se clickar duas vezes antes de retornar a busca
        ui->pushButton->setEnabled(false);
        switch (index) {
        case 0:
            this->draw_table(ui->campoDir1->text()+"/",ui->campoDir2->text()+"/");
            break;
        case 1:
            this->draw_table_content(ui->campoDir1->text()+"/",ui->campoDir2->text()+"/");
            break;
        default:

            break;
        }
        QMessageBox::information(this, "Mensagem", "Consulta realizada com sucesso!", QMessageBox::Ok);
        ui->pushButton->setEnabled(true);
    }

}

QColor MainWindow::nivel_de_semelhanca(float semelhanca){
    QColor cor;
    if(semelhanca == 0){
        cor = QColor(Qt::red);
    }else if(semelhanca == 100){
        cor = QColor(Qt::green);
    }else if(semelhanca == -1){
        cor = QColor(Qt::magenta);
    }else if(semelhanca > 0 && semelhanca <= 20){
        cor = QColor(Qt::yellow);
    }else if(semelhanca > 20 && semelhanca <= 50){
        cor = QColor(Qt::cyan);
    }else{
        cor = QColor(Qt::blue);
    }
    return cor;
}


void MainWindow::on_btnClear_clicked()
{
    this->init_table();
}
