#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>

namespace Ui {
class MainWindow;
}
//Declaracao de classe em c++
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    //metodos publicos
    //no caso o de cima eh o construtor e o debaixo o destrutor. blza?blz
    explicit MainWindow(QWidget *parent = 0);
    QColor nivel_de_semelhanca(float semelhanca);
    ~MainWindow();

private slots:
    //Slots escutam eventos
    void on_buscar1_clicked();

    void on_buscar2_clicked();

    void on_pushButton_clicked();

     void draw_table_content(QString dir1,QString dir2);

    void draw_table(QString dir1,QString dir2);

    void init_table();



    void on_btnClear_clicked();

private:
    //variaveis privadas
    Ui::MainWindow *ui;
     QStandardItemModel *model;
};

#endif // MAINWINDOW_H
