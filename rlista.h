#ifdef __cplusplus
extern "C" {
#endif

#ifndef RLISTA

#define RLISTA

typedef struct no{
    void* info;
    struct no* prox;
}No;

typedef struct{
     No* inicio;
     No* fim;
     int tam;
}RLista;

RLista* criar_lista();

void insere_lista(RLista* RLista,void* info);
void imprimir_lista(RLista* rlista,char* tipo_dado);
No* remove_lista(RLista* rlista,int indice);
No* get_lista(RLista* rlista,int indice);

#endif

#ifdef __cplusplus
}
#endif
