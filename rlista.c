#include <stdio.h>
#include <stdlib.h>

#include "rlista.h"

#define aloca_memoria(x) ((x*) malloc(sizeof(x)))

RLista* criar_lista() {
    RLista* rlista = aloca_memoria(RLista);
    rlista->tam=0;
    rlista->inicio=NULL;
    rlista->fim=NULL;
    return rlista;
}

No* cria_no(void* info) {
    No* no = aloca_memoria(No);
    no->info = info;
    no->prox = NULL;
    return no;
}

void insere_lista(RLista* rlista,void* info) {
    No* novo = cria_no(info);

    if (rlista->tam == 0)  rlista->inicio = novo;
    else
        rlista->fim->prox = novo;
    rlista->fim = novo;
    rlista->tam++;
}

void imprimir_lista(RLista* rlista,char* tipo_dado) {
    No* no;
    if(rlista->tam == 0) {
        printf("\n\t==>Lista vazia!\n");
    } else {
        for(no=rlista->inicio; no!=NULL; no=no->prox) {
            printf(tipo_dado,no->info);
        }
    }
}

No* get_lista(RLista* rlista,int indice) {
    int contador=0;
    No* no;
    if(indice <0 || rlista->tam < indice) return NULL;
    for(no= rlista ->inicio ; no !=NULL; no=no->prox) {
        if(contador++ == indice) return no;
    }
    return NULL;
}

No* remove_lista(RLista* rlista,int indice) {
    No *anterior,*atual,*no;
    anterior = NULL;
    atual=rlista->inicio;
    int contador=0;

    if(indice <0 || rlista->tam < indice) return NULL;

    for (no=rlista->inicio; no!=NULL; no=no->prox) {
        if(contador++ == indice) {
            if(indice == 0) {
                rlista->inicio = atual->prox;
                rlista->tam--;
                return atual;
                //free(atual);
            } else if(indice == rlista->tam -1) {
                rlista->fim = anterior;
                anterior->prox = NULL;
                rlista->tam--;
                return atual;
                //free(atual);
            } else {
                anterior->prox = atual->prox;
                rlista->tam--;
                return atual;
                //free(atual);
            }
        }
        anterior =atual;
        atual = atual->prox;
    }
    return NULL;
}


