#include "rlista.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef RDIR

#define RDIR

int distancia_string(char *s1, char *s2);
float semelhanca_string_percent(char *s1, char *s2);
RLista* get_nome_arquivos(char *diretorio);
float semelhanca_string_percent(char *s1, char *s2);
float compara_arquivos_conteudo(char *name,char* name2);

#endif

#ifdef __cplusplus
}
#endif
